<?php

  function getDirectoryList ($directory) {
    // create an array to hold directory list
    $results = array();
    // create a handler for the directory
    $handler = opendir($directory);
    // open directory and walk through the filenames
    while ($file = readdir($handler)) {
      // if file isn't this directory or its parent, add it to the results
      if ($file != "." && $file != "..") {
        $results[] = $file;
      }
    }
    // tidy up: close the handler
    closedir($handler);
    // done!
    return $results;
  }

  function only_archives($var){
    $var = explode('.',$var);
    $len = count($var);
    return ($var[$len-1] === 'gz' && $var[$len-2] === 'tar');
  }

  $archive = htmlentities($_GET['archive']);

  if ( empty($archive) ) {

    $items = getDirectoryList(getcwd());
    $archives = array_filter($items,'only_archives');
    $message = '';

    if (count($archives) > 0) {

      $message .= '<h1>Select an archive to untar and unzip:</h1>';
      $message .= '<ul>';
      foreach ($archives as $archive) {
        $message .= '<li><a href="?archive='.$archive.'">'.$archive.'</a></li>';
      }
      $message .= '</ul>';
    } else {
      $message .= '<h1>No suitable archive (something.tar.gz) found in this directory</h1>';
    }

  } else if ( file_exists($archive) ){
    exec( 'tar xzf '.$archive );
    $message .= '<h1>'.$archive.' has been successfully decompressed.</h1>';
  } else {
    $host  = $_SERVER['HTTP_HOST']; 
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\'); 
    header("Location: http://$host$uri/decompress.php");
  }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Decompress a Gzipped tarball</title>
    <style>
      html {
        background-color: grey;
      }
      body {
        padding: 2em;
        max-width: 30em;
        margin: 1em auto;
        background-color: #eee;
        font-family: sans-serif;
      }
      h1 {
        font-weight: normal;
      }
    </style>
  </head>
  <body>
    <?php echo $message; ?>
  </body>
</html>